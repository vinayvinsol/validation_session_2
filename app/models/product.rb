class Product < ActiveRecord::Base

  has_many :images
  belongs_to :category

# valid?(context = nil)

# => 1. validate (5 types)

  # => a. validate(context = nil)

  # => b. validate(context = nil)

  # => c. validate(*args, &block)

    # validate :permalink_valid_or_not

    # validate do |product|
    #   product.errors.add(:permalink, "is not a valid permalink format") unless permalink =~ /\A[a-z0-9\-]+\z/i
    # end

    # validate on: :create do
    #   errors.add(:permalink, "is not a valid permalink format") unless permalink =~ /\A[a-z0-9\-]+\z/i
    # end

    # validate :permalink_valid_or_not, unless: -> { permalink.blank? }

  # => d. validate (record)

    #Didnt understood this need to ask!

  # => e. validate!(context = nil)

    #same as validate method it returns true or raises RecordInvalid exception

# => 2. validates *args, custom: true

  # validates :permalink, permalink: true

# => 3. validates_each

  # validates_each :title, :permalink, allow_nil: true do |record, attribute, value|
  #   record.errors.add(attribute, "is not valid") if value.blank?
  # end

# => 4. validates_with (2 types)

  # => a. validates_with(*args, &block) #ClassMethod

    # validates_with MyValidator
    # validates_with MyValidator, my_custom_key: "yoohoo"
    # validates_with MyValidator, MyOtherValidator, my_custom_key: "yoohoo"

  # => b. validates_with(*args, &block) #InstanceMethod

    # validate :hi_there

# => 5. validates_associated

    # validates_associated :category, :images

    # validates_presence_of :category


  private

    def permalink_valid_or_not
      errors.add(:permalink, "is not a valid permalink format") unless permalink =~ /\A[a-z0-9\-]+\z/i
    end

    def hi_there
      validates_with MyValidator, my_custom_key: "yoohoo"
    end

end
