class Image < ActiveRecord::Base
  belongs_to :product
  validate :my_method

  validates_associated :product

  private

    def my_method
      p "we are in image validation"
    end

end
