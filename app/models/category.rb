class Category < ActiveRecord::Base

  has_many :products

  validate :my_method

  private

    def my_method
      p "we are in category validation"
    end

end
