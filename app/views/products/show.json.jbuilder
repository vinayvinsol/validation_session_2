json.extract! @product, :id, :title, :image_url, :permalink, :price, :discount_price, :enabled, :created_at, :updated_at
