json.array!(@products) do |product|
  json.extract! product, :id, :title, :image_url, :permalink, :price, :discount_price, :enabled
  json.url product_url(product, format: :json)
end
