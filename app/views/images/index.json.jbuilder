json.array!(@images) do |image|
  json.extract! image, :id, :url, :product
  json.url image_url(image, format: :json)
end
