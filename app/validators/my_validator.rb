class MyValidator < ActiveModel::Validator

  def validate(record)
    p options[:my_custom_key]
    record.errors.add(:permalink, "is not valid permalink format") unless permalink_vaidate?(record.permalink)
  end

  private

    def permalink_vaidate?(string)
      string =~ /\A[a-z0-9\-]+\z/i
    end

end
