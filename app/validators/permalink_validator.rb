class PermalinkValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    unless value =~ /\A[a-z0-9\-]+\z/i
      record.errors[attribute] << "is not a valid permalink format"
    end
  end

end
