class MyOtherValidator < ActiveModel::Validator

  def validate(record)
    record.errors.add(:permalink, "is not valid permalink other format") unless permalink_vaidate?(record.permalink)
    p options[:my_custom_key]
  end

  private

    def permalink_vaidate?(string)
      string =~ /\A[a-z0-9\-]\z/i
    end

end
