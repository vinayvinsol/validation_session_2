class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.string :image_url
      t.string :permalink
      t.decimal :price
      t.decimal :discount_price
      t.boolean :enabled

      t.timestamps null: false
    end
  end
end
